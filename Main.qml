import QtQuick
import QtQuick.Window
import QtQuick3D

Window {
    width: 1280
    height: 720
    visible: true
    title: qsTr("Typo Dont")


    View3D {
        id: view
        anchors.fill: parent
        environment.antialiasingMode: SceneEnvironment.SSAA
        environment.antialiasingQuality: SceneEnvironment.VeryHigh
        environment.aoEnabled: true
        focus: true

        Keys.onPressed: (event)=> {
                            if (event.key === Qt.Key_Left) {
                                camera_camera.position.x-=10;
                                //camera_camera.lookAt(Qt.vector3d(0,0,0));
                            }
                            else if (event.key === Qt.Key_Right) {
                                camera_camera.position.x+=10;
                                //camera_camera.lookAt(Qt.vector3d(0,0,0));
                            }
                        }

        Node {
            id: node

            // Resources
            PrincipledMaterial {
                id: material_001_material
                baseColor: "#ffcccccc"
                roughness: 0.15
                cullMode: Material.NoCulling

            }

            // Nodes:
            Node {
                id: rootNode
                PointLight {
                    id: light_light
                    position: Qt.vector3d(346.678, 154.825, 742.38)
                    rotation: Qt.quaternion(0.523275, -0.284166, 0.726942, 0.342034)
                    scale: Qt.vector3d(100, 100, 100)
                    brightness: 1
                    quadraticFade: 0.0022222246043384075
                }
                PointLight {
                    id: light2
                    position: Qt.vector3d(-346.678, 154.825, 742.38)
                    rotation: Qt.quaternion(0.523275, -0.284166, 0.726942, 0.342034)
                    scale: Qt.vector3d(100, 100, 100)
                    brightness: 1
                    quadraticFade: 0.0022222246043384075
                }
                Model {
                    id: upperJawScan
                    position: Qt.vector3d(25, 0, -10)
                    rotation: Qt.quaternion(-0.340582, 0.592242, 0.625664, -0.376562)
                    //scale: Qt.vector3d(5, 5, 5)
                    source: "qrc:/meshes/upperJawScan.mesh"
                    materials: [
                        material_001_material,
                        material_001_material
                    ]
                }
                Model {
                    id: lowerJawScan
                    position: Qt.vector3d(0,5,0)
                    rotation: Qt.quaternion(0.603669, 0.795583, -0.0197495, 0.0473418)
                    //scale: Qt.vector3d(5,5,5);
                    source: "qrc:/meshes/lowerJawScan.mesh"
                    materials: [
                        material_001_material,
                        material_001_material
                    ]
                }
                PerspectiveCamera {
                    id: camera_camera
                    position: Qt.vector3d(0, 0, 200)
                    rotation: Qt.quaternion(1, 0, 0, 0)
                    //scale: Qt.vector3d(10, 10, 10)
                    fieldOfView: 40
                    fieldOfViewOrientation: PerspectiveCamera.Horizontal
                    lookAtNode: lowerJawScan

                    SequentialAnimation {
                        running: true
                        PropertyAnimation {
                            from: -250
                            to: +250
                            target: camera_camera
                            property: "position.x"
                            duration: 2000
                        }
                        PropertyAnimation {
                            from: +250
                            to: -250
                            target: camera_camera
                            property: "position.x"
                            duration: 2000
                        }
                        loops: Animation.Infinite
                    }
                }
            }

            // Animations:
        }

    }


    Text {
        text: view.renderStats.fps + "\n" + view.renderStats.graphicsAPIName
    }



}
